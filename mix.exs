defmodule MeiliRsex.MixProject do
  use Mix.Project

  def project do
    [
      app: :meili_rsex,
      version: "0.1.0",
      elixir: "~> 1.16",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {MeiliRsex.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # Rustler
      {:rustler, "~> 0.33.0"}
    ]
  end
end
